
begin transaction;

--area1のふりがな検索用フィールド
--頭３，２，１文字フィールドを更新
update area1 set area1_3chr=substr(area1,1,3);
update area1 set area1_2chr=substr(area1,1,2);
update area1 set area1_1chr=substr(area1,1,1);

update zip set area_3chr=substr(area,1,3);
update zip set area_2chr=substr(area,1,2);
update zip set area_1chr=substr(area,1,1);


--上で設定したフィールド同士を結合し、area_kを取得
update area1 set area1_k=
(select substr(area_k,1,4) from zip z 
where z.area_3chr=area1.area1_3chr)
where area1_k is null;


update area1 set area1_k=(
select substr(z.area_k,1,3) from zip z 
where z.area_2chr=area1.area1_2chr)
where area1_k is null;

update area1 set area1_k=(
select substr(z.area_k,1,2) from zip z 
where z.area_1chr=area1.area1_1chr)
where area1_k is null;


--固定文字列の更新
update area1 set area1_k='ｵｵｱｻﾞ' where substr(area1_3chr,1,2)='大字';
update area1 set area1_k='ｱｻﾞ' where substr(area1_3chr,1,1)='字';

update area1 set area1_k=area1_k || '1'where area1 like  '%一%' ;
update area1 set area1_k=area1_k || '2'where area1 like  '%二%' ;
update area1 set area1_k=area1_k || '3'where area1 like  '%三%' ;
update area1 set area1_k=area1_k || '4'where area1 like  '%四%' ;
update area1 set area1_k=area1_k || '5'where area1 like  '%五%' ;
update area1 set area1_k=area1_k || '6'where area1 like  '%六%' ;
update area1 set area1_k=area1_k || '7'where area1 like  '%七%' ;
update area1 set area1_k=area1_k || '8'where area1 like  '%八%' ;
update area1 set area1_k=area1_k || '9'where area1 like  '%九%' ;


end transaction;

vacuum;
