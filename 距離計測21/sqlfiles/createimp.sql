CREATE TABLE "disdata1" (
	`pref`	TEXT,
	`city`	TEXT,
	`area1`	TEXT,
	`area2`	TEXT,
	`no`	TEXT,
	`x`	NUMERIC,
	`y`	NUMERIC,
	`latitude`	NUMERIC,
	`longitude`	NUMERIC,
	`homeflg`	TEXT,
	`mainflg`	TEXT,
	`updBeforeflg`	TEXT,
	`updAfterflg`	TEXT
);
