﻿Public Class clsImp2SQLite
    Const CONST_BATCH_CNVTOUTF As String = "convert2utf8.bat"
    Const CONST_BATCH_IMPORT As String = "sqliteimp.bat"
    Dim strCurrDirPath As String = Application.StartupPath
    Dim sbText As New System.Text.StringBuilder
    Dim lstUTF8Files As New List(Of String)


    Dim flgDel As Boolean = False

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <param name="_del">削除＝True,削除しない＝False</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal _del As Boolean)
        flgDel = _del
    End Sub

    Public Function imp() As Boolean
        Dim ofd As New OpenFileDialog

        Try

     
            If Not ChkExistsFile() Then Return False

            ofd.Multiselect = True
            If ofd.ShowDialog() <> DialogResult.OK Then Return False

            Dim lstFiles As New List(Of String)
            For Each strfile As String In ofd.FileNames
                lstFiles.Add(strfile)
            Next

            If lstFiles.Count <= 0 Then Return False


            If IO.File.Exists(CONST_BATCH_IMPORT) Then IO.File.Delete(CONST_BATCH_IMPORT)

            If Not CreateCnvToUTF8Batch(lstFiles) Then Return False
            If Not CreateImportBatch(lstUTF8Files) Then Return False

            Dim p As Process
            p = System.Diagnostics.Process.Start(CONST_BATCH_CNVTOUTF)
            p.WaitForExit()

            p = System.Diagnostics.Process.Start(CONST_BATCH_IMPORT)
            p.WaitForExit()

            If Not MoveDB() Then Return False


            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try

    End Function

    ''' <summary>
    ''' 必須ファイル存在確認
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ChkExistsFile() As Boolean
        If Not IO.File.Exists(strCurrDirPath & "/ext/nkfwin32.exe") Then Return False
        If Not IO.File.Exists(strCurrDirPath & "/ext/sqlite3.exe") Then Return False
        Return True

    End Function

    ''' <summary>
    ''' UTF8変換バッチの作成
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateCnvToUTF8Batch(ByVal lstFiles As List(Of String)) As Boolean
        Dim sw As New System.IO.StreamWriter(CONST_BATCH_CNVTOUTF, False, _
                                             System.Text.Encoding.GetEncoding("shift-jis"))
        Dim strFileNameAfter As String
        Try

            sbText.AppendLine("@echo off")
            sbText.AppendFormat("cd {0}" & vbCrLf, strCurrDirPath & "/ext")
            sbText.AppendLine("echo 変換開始 %time%")

            For Each strFile As String In lstFiles
                strFileNameAfter = _
                 "../ext/" & IO.Path.GetFileNameWithoutExtension(strFile) & "_U" & IO.Path.GetExtension(strFile)

                Dim utfenc = System.Text.Encoding.GetEncoding("utf-8")
                Dim tmp() As Byte = utfenc.GetBytes(strFileNameAfter)
                Dim strFileNameUTF8 As String = utfenc.GetString(tmp)

                sbText.AppendFormat("nkfwin32.exe -w {0} > {1}" & vbCrLf, strFile, strFileNameUTF8)



                lstUTF8Files.Add(strFileNameUTF8)

            Next

            sbText.AppendLine("echo 変換終了 %time%")

            sw.WriteLine(sbText.ToString())
            sw.Close()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False

        End Try

    End Function

    ''' <summary>
    ''' インポートバッチ作成
    ''' </summary>
    ''' <param name="lstFiles"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateImportBatch(ByVal lstFiles As List(Of String)) As Boolean
        Dim sw As New System.IO.StreamWriter(CONST_BATCH_IMPORT, False, _
                                             System.Text.Encoding.GetEncoding("shift-jis"))
        Dim sbText As New System.Text.StringBuilder

        Try


            sbText.Remove(0, sbText.Length)

            sbText.AppendLine("@echo off")
            sbText.AppendFormat("cd {0}" & vbCrLf, strCurrDirPath & "/ext")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/createimp.sql")
            sbText.AppendLine("echo インポート開始 %time%")

            For Each strFile As String In lstFiles

                Dim utfenc = System.Text.Encoding.GetEncoding("utf-8")
                Dim tmp() As Byte = utfenc.GetBytes(strFile)
                Dim strFileNameUTF8 As String = utfenc.GetString(tmp)

                sbText.AppendFormat("sqlite3 -separator , ""distance.db"" "".import {0} disdata1""" & vbCrLf, strFileNameUTF8)
                'sbText.AppendFormat("sqlite3 -separator , ""distance.db"" "".import {0} disdata1""" & vbCrLf, strFile)
            Next

            '削除フラグがある場合は削除しないSQLを実行
            If flgDel Then
                sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/imp.sql")
            Else
                sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/imp_nonedel.sql")
            End If

            sbText.AppendLine("echo インポート終了 %time%")

            sbText.AppendLine("echo 都道府県登録 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/insert_pref_city.sql")

            sbText.AppendLine("echo 市区町村ふりがな更新 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/update_city_k.sql")

            sbText.AppendLine("echo 都道府県ID更新 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/update_id_pref.sql")

            sbText.AppendLine("echo 街区更新 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/insert_area1.sql")

            sbText.AppendLine("echo 街区ID更新 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/update_id_area1.sql")

            sbText.AppendLine("echo 街区ふりがな更新 %time%")
            sbText.AppendLine("sqlite3 distance.db < ../sqlfiles/update_area1_k.sql")

            sbText.AppendLine("echo 終了 %time%")

            sbText.AppendLine("pause")

            sw.Write(sbText.ToString())
            sw.Close()


            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

    End Function

    Private Function MoveDB() As Boolean
        Try
            'IO.File.Move("distance.db", "..\distance.db")
            IO.File.Copy(strCurrDirPath & "/ext/" & "distance.db", _
                         strCurrDirPath & "/distance.db", True)
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

    End Function

End Class
