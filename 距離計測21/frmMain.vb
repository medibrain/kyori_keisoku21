﻿Imports System.ComponentModel
Imports System.Data


Public Class frmMain

    Dim pref1 As String = String.Empty
    Dim city1 As String = String.Empty
    Dim pref2 As String = String.Empty
    Dim city2 As String = String.Empty

    '検索対象の都道府県
    Partial Public Class clsPrefecture
        Public strPrefName As String = String.Empty
        Public strPrefDBName As String = String.Empty
        Public strPrefID As Integer = 0
    End Class

    Dim clsPref As New clsPrefecture

#Region "オブジェクトイベント"

#Region "フォームロード"

    '20160315093444 st ////////////////////////
    '検索可能都道府県を表示
    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadInit()

        SetPref()

    End Sub
    '20160315093444 ed ////////////////////////
#End Region

#Region "位置情報取込"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImp.Click
        'Dim clscomm As New clsComm
        'Dim cmd As New SQLite.SQLiteCommand
        'cmd.Connection = clscomm.Conn

        'Dim trans As SQLite.SQLiteTransaction = Nothing
        Dim flgDel As Boolean = True

        'Dim strsql As String = String.Empty
        'Dim da As New SQLite.SQLiteDataAdapter

        'Dim dsCSV As New DataSet

        'Dim clscsv As New clsCSV


        Try
            If MsgBox("現在の位置情報データを削除します。よろしいですか？",
                      MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question + MsgBoxStyle.YesNo) = vbNo Then
                flgDel = False
            End If
            tssl.Text = "取込準備"

            'センシティブ
            EnableCtl(False)

            'バッチファイルにてインポート処理
            Dim clsImp As New clsImp2SQLite(flgDel)
            If Not clsImp.imp() Then Exit Sub

            LoadInit()
            SetPref()

            'センシティブ
            EnableCtl(True)

            ''CSV読込
            'If Not clscsv.LoadCSV(dsCSV) Then Exit Sub

            ''登録準備
            'trans = cmd.Connection.BeginTransaction

            'If flgDel Then
            '    tssl.Text = "クリア"
            '    '一旦全レコード削除
            '    cmd.CommandText = "delete from disdata"
            '    cmd.ExecuteNonQuery()

            '    '20160325145907 st ////////////////////////
            '    '追加テーブル（prefとarea1)の削除
            '    'pref削除
            '    cmd.CommandText = "delete from pref"
            '    cmd.ExecuteNonQuery()

            '    'area1削除
            '    cmd.CommandText = "delete from area1"
            '    cmd.ExecuteNonQuery()
            '    '20160325145907 ed ////////////////////////


            'End If

            ''DataSetに取り込んだCSVをDBに登録
            'For cnt As Integer = 0 To dsCSV.Tables.Count - 1

            '    tspb.Value = 0
            '    tspb.Maximum = dsCSV.Tables(cnt).Rows.Count
            '    tspb.Minimum = 0
            '    System.Windows.Forms.Application.DoEvents()

            '    tssl.Text = dsCSV.Tables(cnt).TableName & "取込"
            '    System.Windows.Forms.Application.DoEvents()

            '    'CSVをテーブルに入れる
            '    For r As Integer = 0 To dsCSV.Tables(cnt).Rows.Count - 1
            '        strsql = ""

            '        '20160325150034 st ////////////////////////
            '        'DisDataテーブル変更に伴う更新フィールドの変更
            '        strsql = "INSERT INTO DISDATA " & _
            '        "(PREF,CITY,AREA1,AREA2,NO,X,Y,LATITUDE,LONGITUDE," & _
            '        "HOMEFLG,MAINFLG,UPDBEFOREFLG,UPDAFTERFLG) " & _
            '        "VALUES ("
            '        'strsql = "insert into disdata values ("
            '        '20160325150034 ed ////////////////////////


            '        For c As Integer = 0 To dsCSV.Tables(cnt).Columns.Count - 1
            '            strsql &= "'" & dsCSV.Tables(cnt).Rows(r)(c).ToString & "'"
            '            If c < dsCSV.Tables(cnt).Columns.Count - 1 Then strsql &= ","
            '        Next

            '        strsql &= ")"
            '        cmd.CommandText = strsql

            '        cmd.ExecuteNonQuery()
            '        tspb.Value += 1
            '        If r Mod 10000 = 0 Then System.Windows.Forms.Application.DoEvents()

            '    Next

            '    tspb.Value = dsCSV.Tables(cnt).Rows.Count

            '    System.Windows.Forms.Application.DoEvents()


            '    '20160325150114 st ////////////////////////
            '    '追加テーブル（prefとarea1)の作成・登録
            '    tssl.Text = dsCSV.Tables(cnt).TableName & "都道府県・市区町村テーブル作成"
            '    System.Windows.Forms.Application.DoEvents()
            '    'Prefテーブル作成
            '    strsql = ""

            '    strsql = "INSERT INTO PREF (PREF,CITY) SELECT PREF,CITY FROM DISDATA " & _
            '    "WHERE NOT EXISTS(SELECT PREF,CITY FROM PREF WHERE DISDATA.PREF=PREF.PREF AND DISDATA.CITY=PREF.CITY) " & _
            '    "GROUP BY PREF,CITY"



            '    cmd.CommandText = strsql
            '    cmd.ExecuteNonQuery()


            '    '//20170609113350 furukawa st ////////////////////////
            '    '//市区町村テーブルにふりがな入れる(リストソート用）
            '    tssl.Text = dsCSV.Tables(cnt).TableName & "市区町村テーブルふりがな"
            '    System.Windows.Forms.Application.DoEvents()
            '    'Prefテーブル作成
            '    strsql = ""

            '    strsql = "update pref set city_k=(select city_k from zip where " & _
            '    "pref.PREF=zip.PREF AND pref.CITY=zip.CITY) "

            '    cmd.CommandText = strsql
            '    cmd.ExecuteNonQuery()

            '    '//20170609113350 furukawa ed ////////////////////////


            '    tssl.Text = dsCSV.Tables(cnt).TableName & "都道府県・市区町村ID設定"
            '    System.Windows.Forms.Application.DoEvents()
            '    'ID_PrefをDisDataにセット
            '    strsql = ""
            '    strsql = "update disdata set id_pref=" & _
            '            "(select id_pref from pref where pref.pref=disdata.pref and pref.city=disdata.city)"
            '    cmd.CommandText = strsql
            '    cmd.ExecuteNonQuery()


            '    tssl.Text = dsCSV.Tables(cnt).TableName & "地域テーブル作成"
            '    System.Windows.Forms.Application.DoEvents()
            '    'Area1テーブル作成
            '    strsql = ""
            '    'strsql = "INSERT INTO AREA1 (ID_PREF,AREA1) SELECT ID_PREF,AREA1 FROM DISDATA " & _
            '    '"WHERE NOT EXISTS(SELECT AREA1 FROM AREA1 WHERE DISDATA.ID_PREF=AREA1.ID_PREF AND DISDATA.AREA1=AREA1.AREA1 GROUP BY ID_PREF,AREA1) " & _
            '    '"GROUP BY ID_PREF,AREA1"

            '    'ExistsよりLeftjoin ISNULLのほうが早い
            '    strsql = "INSERT INTO AREA1 (ID_PREF,AREA1) " & _
            '            "SELECT D.ID_PREF,D.AREA1 FROM DISDATA D " & _
            '            "LEFT JOIN AREA1 A ON D.ID_PREF=A.ID_PREF AND D.AREA1=A.AREA1 " & _
            '            "WHERE A.ID_PREF IS NULL AND A.AREA1 IS NULL " & _
            '            "GROUP BY D.ID_PREF,D.AREA1"



            '    cmd.CommandText = strsql
            '    cmd.ExecuteNonQuery()



            '    Dim dtpref As New DataTable
            '    cmd.CommandText = "SELECT CAST(ID_PREF AS VARCHAR) AS ID_PREF FROM PREF GROUP BY CITY"
            '    da.SelectCommand = cmd
            '    da.Fill(dtpref)

            '    tssl.Text = dsCSV.Tables(cnt).TableName & "地域ID設定"
            '    tspb.Maximum = dtpref.Rows.Count
            '    tspb.Value = 0
            '    System.Windows.Forms.Application.DoEvents()
            '    For r As Integer = 0 To dtpref.Rows.Count - 1
            '        'ID_Area1をDisDataにセット
            '        strsql = ""
            '        strsql = "UPDATE DISDATA SET ID_AREA1=" & _
            '                "(SELECT ID_AREA1 FROM AREA1 " & _
            '                    "WHERE AREA1.ID_PREF=DISDATA.ID_PREF " & _
            '                    "AND AREA1.AREA1=DISDATA.AREA1) " & _
            '                "WHERE DISDATA.ID_PREF=" & dtpref.Rows(r)("ID_PREF")

            '        cmd.CommandText = strsql
            '        tssl.Text = dsCSV.Tables(cnt).TableName & "地域ID設定" & cmd.ExecuteNonQuery() & "件"
            '        tspb.Value += 1
            '        System.Windows.Forms.Application.DoEvents()
            '    Next

            '    dtpref.Dispose()
            '    '20160325150114 ed ////////////////////////
            '    clscsv.CloseCSV()

            'Next

            'trans.Commit()
            tssl.Text = "取込完了"

        Catch ex As Exception
            tssl.Text &= "エラー"
            MsgBox(ex.Message)
            ' If Not IsNothing(trans) Then trans.Rollback()

        Finally
            EnableCtl(True)

            ' clscomm.Disconn()
            ' clscomm = Nothing
            tspb.Value = 0
            dtp.Dispose()

            LoadInit()

            SetPref()

        End Try

    End Sub

#End Region

#Region "郵便番号取込"


    Private Sub btnZip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnZip.Click
        Dim clscomm As New clsComm
        Dim cmd As New SQLite.SQLiteCommand
        cmd.Connection = clscomm.Conn()

        Dim olecn As New OleDb.OleDbConnection
        Dim olecmd As New OleDb.OleDbCommand
        Dim oleda As New OleDb.OleDbDataAdapter
        Dim dt As New DataTable
        Dim trans As SQLite.SQLiteTransaction = Nothing

        Try
            If MsgBox("現在の郵便番号データを削除します。よろしいですか？",
                MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question + MsgBoxStyle.YesNo) = vbNo Then Exit Sub

            ofd.Title = "郵便番号CSVを選択してください"
            ofd.Filter = "CSVファイル|*.csv"
            If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Sub

            EnableCtl(False)

            olecn.ConnectionString = "provider=microsoft.jet.oledb.4.0;" &
            "data source=" & IO.Path.GetDirectoryName(ofd.FileName) & ";" &
            "extended properties=""TEXT;HDR=YES;FMT=Delimited"""
            olecn.Open()
            olecmd.Connection = olecn
            olecmd.CommandText = "select * from " & IO.Path.GetFileName(ofd.FileName)
            oleda.SelectCommand = olecmd
            System.Windows.Forms.Application.DoEvents()

            oleda.Fill(dt)

            trans = cmd.Connection.BeginTransaction
            tspb.Maximum = dt.Rows.Count - 1
            tspb.Minimum = 0
            tssl.Text = "削除"
            cmd.CommandText = "delete from Zip"
            cmd.ExecuteNonQuery()

            tssl.Text = "取込"
            Dim strsql As String = String.Empty
            For r As Integer = 0 To dt.Rows.Count - 1
                strsql = ""
                strsql = "insert into Zip values ("

                For c As Integer = 0 To 8
                    strsql &= "'" & dt.Rows(r)(c).ToString & "'"
                    If c < 8 Then strsql &= ","
                Next
                strsql &= ")"
                cmd.CommandText = strsql

                cmd.ExecuteNonQuery()
                tspb.Value = r
                If r Mod 50000 = 0 Then System.Windows.Forms.Application.DoEvents()

            Next

            'cmd.CommandText = "delete from Zip where pref not in('福井県','京都府','石川県','滋賀県','岐阜県')"
            'cmd.ExecuteNonQuery()

            trans.Commit()
            tssl.Text = "取込完了"

        Catch ex As Exception
            MsgBox(ex.Message)
            If Not IsDBNull(trans) OrElse Not IsNothing(trans) Then trans.Rollback()
        Finally
            olecn.Close()
            clscomm.Disconn()
            clscomm = Nothing
            tspb.Value = 0
            EnableCtl(True)


        End Try

    End Sub
#End Region

#Region "郵便番号テキスト"

    Private Sub txtZip1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
                                Handles txtZip1.KeyUp, txtZip2.KeyUp


        If DirectCast(sender, MaskedTextBox).Text.Length = 8 Then
            Select Case DirectCast(sender, MaskedTextBox).Name

                Case "txtZip1"
                    AddressSearchZip(sender, txtAdd1)
                    CreateSource(sender, txtAdd1, True)
                    ' CreateSourceSejutu()

                Case "txtZip2"
                    AddressSearchZip(sender, txtAdd2)
                    CreateSource(sender, txtAdd2, True)
            End Select

            SendKeys.Send("{TAB}")

        End If
    End Sub


#End Region

#Region "距離計測ボタン"

    '距離計測
    Private Sub btnCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalc.Click
        Dim clscomm As New clsComm
        Dim cmd As New SQLite.SQLiteCommand
        Dim sb As New System.Text.StringBuilder
        Dim strID As String = String.Empty
        Dim trans As SQLite.SQLiteTransaction = Nothing
        Dim dt As New DataTable
        Dim da As New SQLite.SQLiteDataAdapter

        'If txtAdd1.Text = String.Empty Then Exit Sub
        'If txtAdd2.Text = String.Empty Then Exit Sub

        Try
            chkKm.Checked = False


            '初期化
            lbl1.Text = String.Empty
            lbl2.Text = String.Empty



            '距離計算
            If IsNumeric(txtZip1.Text.Replace("-", "") & txtZip2.Text.Replace("-", "")) Then

                If Not CalcDisZip() Then Exit Sub
            Else

                If Not CalcDis() Then Exit Sub

            End If

            'ログ記録
            cmd.Connection = clscomm.Conn
            trans = cmd.Connection.BeginTransaction

            '最大ID取得
            cmd.CommandText = "select max(substr(ID,9,4)) from log where substr(crdttm,1,10)='" & Now.ToString("yyyy/MM/dd") & "'"
            If IsDBNull(cmd.ExecuteScalar) Then
                strID = 0
            Else
                strID = cmd.ExecuteScalar
            End If
            strID = (Integer.Parse(strID) + 1).ToString

            'テーブル登録
            sb.Remove(0, sb.ToString.Length)
            sb.AppendLine(" insert into log values(")
            sb.AppendFormat(" '{0}','{1}','{2}','{3}','{4}','{5}',",
                            Now.ToString("yyyyMMdd") & Integer.Parse(strID).ToString("0000"),
                            Me.txtZip1.Text, Me.txtAdd1.Text, Me.txtZip2.Text,
                            Me.txtAdd2.Text, Double.Parse(Me.lblRes.Text))
            sb.AppendFormat("'{0}','{1}','{2}')",
                            Now.ToString("yyyy/MM/dd HH:mm:ss"),
                            System.Environment.UserName,
                            System.Environment.MachineName)

            cmd.CommandText = sb.ToString
            cmd.ExecuteNonQuery()

            sb.Remove(0, sb.ToString.Length)
            sb.AppendLine(" select ")

            '//20170620161851 furukawa st ////////////////////////
            '//ログの郵便番号を削除（使用しない）

            'sb.AppendLine(" zip_st as 郵便番号1, ")

            sb.AppendLine(" add_st as 住所1, ")

            'sb.AppendLine(" zip_ed as 郵便番号2, ")

            '//20170620161851 furukawa ed ////////////////////////

            sb.AppendLine(" add_ed as 住所2, ")
            sb.AppendLine(" distance as 距離m ")
            sb.AppendLine(" from log ")
            sb.AppendFormat(" where substr(crdttm,1,10)='{0}'", Now.ToString("yyyy/MM/dd"))
            sb.AppendLine(" order by crdttm desc ")

            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd
            da.Fill(dt)

            Me.dg.DataSource = dt
            Me.dg.AutoResizeColumns()
            Me.dg.Columns("距離m").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Me.dg.Columns("距離m").DefaultCellStyle.Format = "#,0.000"

            trans.Commit()


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            trans.Rollback()

        Finally
            cmd.Dispose()
            clscomm.Disconn()
            clscomm = Nothing
        End Try
    End Sub


#End Region

#Region "キロ表示チェックボックス"
    Private Sub chkKm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkKm.Click
        DisCng()
    End Sub
#End Region

#Region "ログ"

    'ログボタン
    Private Sub btnLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLog.Click
        Dim clscomm As New clsComm
        clscomm.OutLog(Me.dtp.Value)
        clscomm = Nothing
    End Sub
#End Region

#Region "テキストボックスKeyDown(未使用)"

    Private Sub txtAdd1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
                Handles txtAdd1.KeyDown, txtAdd2.KeyDown, txtZip1.KeyDown, txtZip2.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter, Keys.Return

                ' AddressSearchEx()

                'SendKeys.Send("{TAB}")
        End Select
    End Sub
#End Region

#Region "DataGridViewクリック"

    'Private Sub dg_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg.CellClick
    '    If Me.dg.CurrentRow.Cells("郵便番号1").Value.ToString <> String.Empty Then Me.txtZip1.Text = Me.dg.CurrentRow.Cells("郵便番号1").Value.ToString
    '    If Me.dg.CurrentRow.Cells("郵便番号2").Value.ToString <> String.Empty Then Me.txtZip2.Text = Me.dg.CurrentRow.Cells("郵便番号2").Value.ToString
    '    If Me.dg.CurrentRow.Cells("住所1").Value.ToString <> String.Empty Then Me.txtAdd1.Text = Me.dg.CurrentRow.Cells("住所1").Value.ToString
    '    If Me.dg.CurrentRow.Cells("住所2").Value.ToString <> String.Empty Then Me.txtAdd2.Text = Me.dg.CurrentRow.Cells("住所2").Value.ToString


    'End Sub
#End Region

#Region "県変更イベント"
    Private Sub cmbPref1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPref1.SelectedIndexChanged

        SetCity(cmbCity1, cmbPref1.Text)

    End Sub

    Private Sub cmbPref2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPref2.SelectedIndexChanged

        SetCity(cmbCity2, cmbPref2.Text)

    End Sub

    Private Sub cmbPref1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPref1.SelectedValueChanged
        ' SetCity(cmbCity1, cmbPref1.Text)
    End Sub

    Private Sub cmbPref1_Validated(sender As Object, e As EventArgs) Handles cmbPref1.Validated
        SetCity(cmbCity1, cmbPref1.Text)
        clsPref.strPrefDBName = GetPrefDBName(cmbPref1.Text)
        'cmbCity1.Select()

    End Sub



    Private Sub cmbPref2_Validated(sender As Object, e As EventArgs) Handles cmbPref2.Validated
        SetCity(cmbCity2, cmbPref2.Text)
        clsPref.strPrefDBName = GetPrefDBName(cmbPref2.Text)
        'cmbCity2.Select()

    End Sub

#End Region

#Region "市変更イベント"

    Private Sub cmbCity1_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCity1.SelectionChangeCommitted
        SetArea1(Me.cmbArea11, Me.cmbCity1)
        'SetArea1(Me.cmbArea11, Me.cmbCity1.SelectedValue)
    End Sub
    Private Sub cmbCity1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCity1.SelectedIndexChanged
        'SetArea1(Me.cmbArea11, Me.cmbCity1.SelectedValue)
    End Sub


    Private Sub cmbCity1_Validated(sender As Object, e As EventArgs) Handles cmbCity1.Validated
        SetArea1(Me.cmbArea11, Me.cmbCity1.Text)
        'SetArea1(Me.cmbArea11, Me.cmbCity1.SelectedValue)
        'cmbArea11.Select()
    End Sub

    Private Sub cmbCity2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCity2.SelectedIndexChanged
        'SetArea1(Me.cmbArea21, Me.cmbCity2.SelectedValue)
    End Sub

    Private Sub cmbCity2_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCity2.SelectionChangeCommitted
        SetArea1(Me.cmbArea21, Me.cmbCity2)
        'SetArea1(Me.cmbArea21, Me.cmbCity2.SelectedValue)
    End Sub


    Private Sub cmbCity2_Validated(sender As Object, e As EventArgs) Handles cmbCity2.Validated
        SetArea1(Me.cmbArea21, Me.cmbCity2)
        'SetArea1(Me.cmbArea21, Me.cmbCity2.Text)
        'SetArea1(Me.cmbArea21, Me.cmbCity2.SelectedValue)
        'cmbArea21.Select()
    End Sub

#End Region

#Region "Area1変更イベント"

    Private Sub cmbArea11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbArea11.SelectedIndexChanged
        SetArea2(Me.cmbArea12, Me.cmbArea11, Me.cmbCity1.Text)
        'SetArea2(Me.cmbArea12, Me.cmbArea11, Me.cmbCity1.SelectedValue.ToString)
    End Sub
    Private Sub cmbArea21_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbArea21.SelectedIndexChanged
        SetArea2(Me.cmbArea22, Me.cmbArea21, Me.cmbCity2.Text)
        'SetArea2(Me.cmbArea22, Me.cmbArea21, Me.cmbCity2.SelectedValue.ToString)
    End Sub
    Private Sub cmbArea11_Validated(sender As Object, e As EventArgs) Handles cmbArea11.Validated
        SetArea2(Me.cmbArea12, Me.cmbArea11, Me.cmbCity1.SelectedValue)
        'cmbArea12.Select()
    End Sub

    Private Sub cmbArea21_Validated(sender As Object, e As EventArgs) Handles cmbArea21.Validated
        SetArea2(Me.cmbArea22, Me.cmbArea21, Me.cmbCity2.SelectedValue)
        '        cmbArea22.Select()

    End Sub
#End Region

#Region "Area2変更イベント"

    Private Sub cmbArea12_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbArea12.SelectedIndexChanged
        pref1 = Me.cmbPref1.Text
        city1 = Me.cmbCity1.Text
        Me.txtAdd1.Text = pref1 & city1 & Me.cmbArea11.Text & Me.cmbArea12.Text

    End Sub
    Private Sub cmbArea22_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbArea22.SelectedIndexChanged
        pref2 = Me.cmbPref2.Text
        city2 = Me.cmbCity2.Text
        Me.txtAdd2.Text = pref2 & city2 & Me.cmbArea21.Text & Me.cmbArea22.Text

    End Sub

    Private Sub cmbArea12_Validated(sender As Object, e As EventArgs) Handles cmbArea12.Validated
        pref1 = Me.cmbPref1.Text
        city1 = Me.cmbCity1.Text
        Me.txtAdd1.Text = pref1 & city1 & Me.cmbArea11.Text & Me.cmbArea12.Text
    End Sub

    Private Sub cmbArea22_Validated(sender As Object, e As EventArgs) Handles cmbArea22.Validated
        pref2 = Me.cmbPref2.Text
        city2 = Me.cmbCity2.Text
        Me.txtAdd2.Text = pref2 & city2 & Me.cmbArea21.Text & Me.cmbArea22.Text

    End Sub
#End Region



#End Region

#Region "関数"

#Region "住所検索"
    Private Sub AddressSearchZip(ByVal sender As MaskedTextBox, ByVal ctl As TextBox)
        Dim clsComm As New clsComm
        Dim cmd As New SQLite.SQLiteCommand
        Dim dt As New DataTable
        Dim da As New SQLite.SQLiteDataAdapter
        Try

            cmd.Connection = clsComm.Conn()

            '20160315114309 st ////////////////////////
            '住所取得時、県、市を取得
            cmd.CommandText = "select pref,city,pref || city || area  as addr from zip where zip='" & Replace(sender.Text, "-", "") & "'"
            'cmd.CommandText = "select pref || city || area  as addr from zip where zip='" & Replace(sender.Text, "-", "") & "'"
            '20160315114309 ed ////////////////////////

            '20160315114342 st ////////////////////////
            '県、市を個別に取得し、距離検索の条件で使用する

            da.SelectCommand = cmd
            da.Fill(dt)


            If dt.Rows.Count = 0 Then
                ctl.Text = String.Empty
                Exit Sub
            End If

            ctl.Text = dt.Rows(0)("addr")
            Select Case sender.Name
                Case Me.txtZip1.Name
                    pref1 = dt.Rows(0)("pref").ToString
                    city1 = dt.Rows(0)("city").ToString

                Case Me.txtZip2.Name
                    pref2 = dt.Rows(0)("pref").ToString
                    city2 = dt.Rows(0)("city").ToString

            End Select
            'ctl.Text = cmd.ExecuteScalar()
            '20160315114342 ed ////////////////////////

        Catch ex As Exception
            ctl.Text = String.Empty

        Finally
            clsComm.Disconn()
            clsComm = Nothing

        End Try
    End Sub


    Private Sub AddressSearch(ByVal sender As MaskedTextBox, ByVal ctl As TextBox)
        Dim clsComm As New clsComm
        Dim cmd As New SQLite.SQLiteCommand
        Dim dt As New DataTable
        Dim da As New SQLite.SQLiteDataAdapter
        Try

            cmd.Connection = clsComm.Conn()

            '20160315114309 st ////////////////////////
            '住所取得時、県、市を取得
            cmd.CommandText = "select pref,city,pref || city || area  as addr from zip where zip='" & Replace(sender.Text, "-", "") & "'"
            'cmd.CommandText = "select pref || city || area  as addr from zip where zip='" & Replace(sender.Text, "-", "") & "'"
            '20160315114309 ed ////////////////////////

            '20160315114342 st ////////////////////////
            '県、市を個別に取得し、距離検索の条件で使用する
            da.SelectCommand = cmd
            da.Fill(dt)


            If dt.Rows.Count = 0 Then
                ctl.Text = String.Empty
                Exit Sub
            End If

            ctl.Text = dt.Rows(0)("addr")
            Select Case sender.Name
                Case Me.txtZip1.Name
                    pref1 = dt.Rows(0)("pref").ToString
                    city1 = dt.Rows(0)("city").ToString

                Case Me.txtZip2.Name
                    pref2 = dt.Rows(0)("pref").ToString
                    city2 = dt.Rows(0)("city").ToString

            End Select
            'ctl.Text = cmd.ExecuteScalar()
            '20160315114342 ed ////////////////////////

        Catch ex As Exception
            ctl.Text = String.Empty

        Finally
            clsComm.Disconn()
            clsComm = Nothing

        End Try
    End Sub


#End Region

#Region "距離算出"

    '距離を出す
    Private Function CalcDis() As Boolean
        Dim clsDis As New clsCalcDistance
        Dim clsComm As New clsComm
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim sb As New System.Text.StringBuilder
        Dim da As New SQLite.SQLiteDataAdapter
        Dim dt As New DataTable

        Dim IDO1 As Double = 0.0
        Dim KEIDO1 As Double = 0.0
        Dim IDO2 As Double = 0.0
        Dim KEIDO2 As Double = 0.0

        Dim dis As Double = 0.0



        Try


            cn = clsComm.Conn

            '2点の緯度、経度の取得
            sb.Remove(0, sb.ToString.Length)

            '20160315114134 st ////////////////////////
            '距離取得の条件を県、市、それ以下に分けてANDにした（速度向上）

            ' If txtZip1.Text = String.Empty And txtZip2.Text = String.Empty Then


            sb.AppendFormat("SELECT distinct LATITUDE,LONGITUDE FROM {0} WHERE address_all='{1}' ",
                                clsPref.strPrefDBName, Me.txtAdd1.Text)
            sb.AppendLine(" UNION ALL ")

            sb.AppendFormat("SELECT distinct LATITUDE,LONGITUDE FROM {0} WHERE address_all='{1}'",
                                clsPref.strPrefDBName, Me.txtAdd2.Text)



            'sb.AppendFormat("SELECT LATITUDE,LONGITUDE FROM DISDATA WHERE PREF ='{0}' AND CITY ='{1}' AND AREA1 || AREA2 = '{2}' AND MAINFLG='1'",
            '                    pref1, city1, Me.txtAdd1.Text.Substring(pref1.Length + city1.Length))
            '    sb.AppendLine(" UNION ALL ")

            '    sb.AppendFormat("SELECT LATITUDE,LONGITUDE FROM DISDATA WHERE PREF ='{0}' AND CITY ='{1}' AND AREA1 || AREA2 = '{2}' AND MAINFLG='1'",
            '                    pref2, city2, Me.txtAdd2.Text.Substring(pref2.Length + city2.Length))






            'Else

            '    sb.AppendLine("select distinct ")
            '    sb.AppendLine("d.pref,d.city,d.area1,cast(d.area2 as int) ")
            '    sb.AppendLine(",d.latitude,d.longitude  ")
            '    sb.AppendLine("from   ")
            '    sb.AppendFormat("{0} d inner join ", clsPref.strPrefDBName)
            '    sb.AppendLine("zip z ")
            '    sb.AppendLine("on ")
            '    sb.AppendLine("d.pref=z.pref and d.city=z.city  and substr(d.area1,1,3)=z.area_3 ")
            '    sb.AppendFormat("where d.address_all='{0}' ", txtAdd1.Text)
            '    'sb.AppendFormat("where d.pref || d.city || d.area1 || d.area2= '{0}' ", txtAdd1.Text)

            '    'sb.AppendFormat("where z.zip='{0}' ", txtZip1.Text.Replace("-", String.Empty))
            '    sb.AppendLine("and d.mainflg='1' ")


            '    '    sb.AppendLine("select LATITUDE,LONGITUDE from disdata d ")
            '    'sb.AppendLine("inner join zip z on  ")
            '    'sb.AppendLine("d.pref=z.pref and  ")
            '    'sb.AppendLine("d.city=z.city and  ")
            '    'sb.AppendLine("substr(d.area1,1,2)=z.area_2 ")
            '    'sb.AppendFormat("where z.pref || z.city || z.area ={0} ", txtAdd1.Text)



            '    sb.AppendLine(" UNION ALL ")

            '    sb.AppendLine("select distinct ")
            '    sb.AppendLine("d.pref,d.city,d.area1,cast(d.area2 as int) ")
            '    sb.AppendLine(",d.latitude,d.longitude  ")
            '    sb.AppendLine("from   ")
            '    sb.AppendFormat("{0} d inner join ", clsPref.strPrefDBName)
            '    sb.AppendLine("zip z ")
            '    sb.AppendLine("on ")
            '    sb.AppendLine("d.pref=z.pref and d.city=z.city  and substr(d.area1,1,3)=z.area_3 ")
            '    sb.AppendFormat("where d.address_all= '{0}' ", txtAdd2.Text)
            '    'sb.AppendFormat("where d.pref || d.city || d.area1 || d.area2= '{0}' ", txtAdd2.Text)
            '    sb.AppendLine("and d.mainflg='1' ")
            '    'sb.AppendLine("select LATITUDE,LONGITUDE from disdata d ")
            '    'sb.AppendLine("inner join zip z on  ")
            '    'sb.AppendLine("d.pref=z.pref and  ")
            '    'sb.AppendLine("d.city=z.city and  ")
            '    'sb.AppendLine("substr(d.area1,1,2)=z.area_2 ")
            '    'sb.AppendFormat("where z.pref || z.city || z.area ={0} ", txtAdd2.Text)


            '    sb.AppendLine("order by d.pref,d.city,d.area1,cast(d.area2 as int) ")

            '    'sb.AppendLine("order by area1,cast(area2 as int) ")
            'End If


            'sb.AppendLine("select latitude,longitude from disdata  where  pref || city || area1 || area2 = ")
            'sb.AppendFormat("'{0}' and mainflg='1'", Me.txtAdd1.Text.Trim)
            'sb.AppendLine(" union all ")
            'sb.AppendLine("select latitude,longitude from disdata  where  pref || city || area1 || area2 like ")
            'sb.AppendFormat("'{0}%' and mainflg='1'", Me.txtAdd2.Text.Trim)
            '20160315114134 ed ////////////////////////

            cmd.Connection = cn
            cmd.CommandText = sb.ToString









            Dim dr As SQLite.SQLiteDataReader
            dr = cmd.ExecuteReader()
            dr.Read()
            Try
                IDO1 = Double.Parse(dr("latitude").ToString)
                KEIDO1 = Double.Parse(dr("longitude").ToString)
                lbl1.Text = IDO1 & "," & KEIDO1

            Catch ex As Exception
                lbl1.Text = "取得できず"
                dr.Close()
                Return False
            Finally

            End Try
            dr.Read()
            Try
                IDO2 = Double.Parse(dr("latitude").ToString)
                KEIDO2 = Double.Parse(dr("longitude").ToString)
                lbl2.Text = IDO2 & "," & KEIDO2
            Catch ex As Exception
                lbl2.Text = "取得できず"
                dr.Close()
                Return False
            Finally
                dr.Close()
            End Try


            'da.SelectCommand = cmd
            'da.Fill(dt)



            ''1行目が開始、2行目が到達地点
            'Try
            '    IDO1 = Double.Parse(dt.Rows(0)("latitude").ToString)
            '    KEIDO1 = Double.Parse(dt.Rows(0)("longitude").ToString)
            '    lbl1.Text = IDO1 & "," & KEIDO1

            'Catch ex As Exception
            '    lbl1.Text = "取得できず"

            '    Return False

            'End Try

            'Try
            '    IDO2 = Double.Parse(dt.Rows(1)("latitude").ToString)
            '    KEIDO2 = Double.Parse(dt.Rows(1)("longitude").ToString)
            '    lbl2.Text = IDO2 & "," & KEIDO2

            'Catch ex As Exception
            '    lbl2.Text = "取得できず"

            '    Return False

            'End Try










            '計算
            dis = clsDis.ComF_CalDistance(IDO1, KEIDO1, IDO2, KEIDO2)
            Me.lblRes.Text = dis

            If chkKm.Checked Then
                Me.lblRes.Text = Double.Parse((dis / 1000.0)).ToString("#,0.000")
                Me.lblM.Text = "Km"
            Else
                Me.lblRes.Text = dis.ToString("#,0")
                Me.lblM.Text = "m"
            End If

            If dis <= 0 Then
                Return False
            End If
            Return True

        Catch ex As Exception
            MsgBox("距離が取得できません" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
        Finally
            dt.Dispose()

            da.Dispose()
            cmd.Dispose()
            clsDis = Nothing
            clsComm.Disconn()
            clsComm = Nothing
        End Try
    End Function


    Private Function CalcDisZip() As Boolean
        Dim clsDis As New clsCalcDistance
        Dim clsComm As New clsComm
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim sb As New System.Text.StringBuilder

        Dim IDO1 As Double = 0.0
        Dim KEIDO1 As Double = 0.0
        Dim IDO2 As Double = 0.0
        Dim KEIDO2 As Double = 0.0

        Dim dis As Double = 0.0


        Dim dr As SQLite.SQLiteDataReader

        Try


            cn = clsComm.Conn

            '2点の緯度、経度の取得
            sb.Remove(0, sb.ToString.Length)

            'zip1
            sb.AppendFormat("select latitude,longitude from disdata where address_all='{0}'", txtAdd1.Text)


            cmd.Connection = cn
            cmd.CommandText = sb.ToString
            dr = cmd.ExecuteReader()
            dr.Read()


            '1行目が開始、2行目が到達地点
            Try

                IDO1 = Double.Parse(dr("latitude").ToString())
                KEIDO1 = Double.Parse(dr("longitude").ToString)
                lbl1.Text = IDO1 & "," & KEIDO1

            Catch ex As Exception
                lbl1.Text = "取得できず"

                Return False
            Finally
                dr.Close()
            End Try

            '2点の緯度、経度の取得
            sb.Remove(0, sb.ToString.Length)

            'zip1
            sb.AppendFormat("select latitude,longitude from disdata where address_all='{0}'", txtAdd2.Text)


            cmd.Connection = cn
            cmd.CommandText = sb.ToString
            dr = cmd.ExecuteReader()
            dr.Read()


            Try
                IDO2 = Double.Parse(dr("latitude").ToString)
                KEIDO2 = Double.Parse(dr("longitude").ToString)
                lbl2.Text = IDO2 & "," & KEIDO2

            Catch ex As Exception
                lbl2.Text = "取得できず"

                Return False
            Finally
                dr.Close()

            End Try


            '計算
            dis = clsDis.ComF_CalDistance(IDO1, KEIDO1, IDO2, KEIDO2)
            Me.lblRes.Text = dis

            If chkKm.Checked Then
                Me.lblRes.Text = Double.Parse((dis / 1000.0)).ToString("#,0.000")
                Me.lblM.Text = "Km"
            Else
                Me.lblRes.Text = dis.ToString("#,0")
                Me.lblM.Text = "m"
            End If

            If dis <= 0 Then
                Return False
            End If
            Return True

        Catch ex As Exception
            MsgBox("距離が取得できません" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
        Finally



            cmd.Dispose()
            cn.Close()

            clsDis = Nothing
            clsComm.Disconn()
            clsComm = Nothing
        End Try
    End Function

#End Region

    'Private Overloads Function GetPref() As String
    '    Dim clsComm As New clsComm
    '    Dim cn As New SQLite.SQLiteConnection
    '    Dim cmd As New SQLite.SQLiteCommand
    '    Dim sb As New System.Text.StringBuilder
    '    Try
    '        cn = clsComm.Conn
    '        Dim strPref As String = String.Empty

    '        sb.Remove(0, sb.ToString.Length)
    '        sb.AppendLine(" SELECT pref_alpha ")
    '        sb.AppendLine(" from pref_zip3 ")
    '        sb.AppendLine(" where  ")
    '        sb.AppendFormat(" zip3='{0}' ", txtZip1.Text.Substring(0, 3))

    '        cmd.Connection = cn
    '        cmd.CommandText = sb.ToString

    '        Return cmd.ExecuteScalar()

    '    Catch ex As Exception

    '    Finally
    '        cmd.Dispose()
    '        clsComm.Disconn()
    '        clsComm = Nothing

    '    End Try

    'End Function

    Private Overloads Function GetPrefDBName(txt As String) As String
        Dim clsComm As New clsComm
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim sb As New System.Text.StringBuilder
        Try
            cn = clsComm.Conn

            If Not IsNumeric(txt.Substring(0, 3)) Then

                sb.Remove(0, sb.ToString.Length)
                sb.AppendLine(" SELECT distinct pref_alpha ")
                sb.AppendLine(" from pref_zip3 ")
                sb.AppendLine(" where  ")
                sb.AppendFormat(" pref='{0}' ", txt)
            Else
                sb.Remove(0, sb.ToString.Length)
                sb.AppendLine(" SELECT pref_alpha ")
                sb.AppendLine(" from pref_zip3 ")
                sb.AppendLine(" where  ")
                sb.AppendFormat(" zip3='{0}' ", txtZip1.Text.Substring(0, 3))

            End If

            cmd.Connection = cn
            cmd.CommandText = sb.ToString

            Return cmd.ExecuteScalar()

        Catch ex As Exception

        Finally
            cmd.Dispose()
            clsComm.Disconn()
            clsComm = Nothing

        End Try

    End Function

#Region "オートコンプリートソース"
    ''' <summary>
    ''' オートコンプリートソースの設定
    ''' </summary>
    ''' <param name="txtZip">郵便番号テキストボックス</param>
    ''' <param name="txtAdd">住所用テキストボックス</param>
    ''' <param name="flgZip">郵便番号を指定した場合＝True</param>
    ''' <remarks></remarks>
    Private Sub CreateSource(ByVal txtZip As MaskedTextBox,
                             ByVal txtAdd As TextBox,
                             Optional ByVal flgZip As Boolean = False)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim cs As New AutoCompleteStringCollection
        Dim clsComm As New clsComm
        Dim dr As SQLite.SQLiteDataReader
        Dim sb As New System.Text.StringBuilder

        Try
            cn = clsComm.Conn


            sb.Remove(0, sb.ToString.Length)

            sb.AppendLine(" SELECT Z.ZIP ")
            sb.AppendLine(",D.address_all ")
            'sb.AppendLine(",D.PREF, D.CITY,D.AREA1,D.AREA2 ")
            sb.AppendFormat(" FROM {0} D ", clsPref.strPrefDBName)

            '20160315093345 st ////////////////////////
            'left join ->inner join に変更
            sb.AppendLine(" inner join zip z ")
            'sb.AppendLine(" left join zip z ")
            '20160315093345 ed ////////////////////////

            sb.AppendLine(" ON Z.PREF=D.PREF AND ")
            sb.AppendLine(" Z.CITY=D.CITY AND ")
            sb.AppendLine(" SUBSTR(Z.AREA,1,3)=SUBSTR(D.AREA1,1,3) ")

            If flgZip Then
                sb.AppendFormat(" WHERE Z.ZIP='{0}'", txtZip.Text.Replace("-", String.Empty))
            End If

            sb.AppendLine(" order by cast(d.area2 as int)")

            cmd.Connection = cn
            cmd.CommandText = sb.ToString

            'da.SelectCommand = cmd

            'Dim lst As New List(Of String)


            Me.Cursor = Cursors.WaitCursor



            dr = cmd.ExecuteReader
            txtAdd.AutoCompleteCustomSource.Clear()

            Do While dr.Read
                '   lst.Add(dr("address_all"))
                'cs.Add(dr("address_all").ToString)
                'cs.Add(dr("pref").ToString & dr("city").ToString &
                'dr("area1").ToString & dr("area2").ToString)
                txtAdd.AutoCompleteCustomSource.Add(dr("address_all"))
            Loop
            dr.Close()
            'txtAdd.AutoCompleteCustomSource = cs
            txtAdd.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            txtAdd.AutoCompleteSource = AutoCompleteSource.CustomSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub
        Finally
            cmd.Dispose()
            clsComm.Disconn()
            clsComm = Nothing
            Me.Cursor = Cursors.Default

        End Try
    End Sub

#End Region

#Region "施術所検索"
    Private Sub CreateSourceSejutu()
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim dt As New DataTable
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder

        Try


            cn = clsComm.Conn

            sb.Remove(0, sb.ToString.Length)
            sb.AppendLine(" select Sejutu_NM ")
            sb.AppendLine("  ")
            sb.AppendLine(" from Sejutu ")
            sb.AppendLine("  ")
            sb.AppendLine("  ")
            sb.AppendFormat(" where zip='{0}'", txtZip1.Text.Replace("-", String.Empty))
            sb.AppendLine(" order by Sejutu_NM")

            cmd.Connection = cn
            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd
            da.Fill(dt)

            cmbSejutu.DataSource = dt
            cmbSejutu.DisplayMember = "Sejutu_NM"


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub
        Finally
            cmd.Dispose()
            da.Dispose()
            clsComm.Disconn()

        End Try
    End Sub

#End Region

#Region "キロ・メートル切り替え"

    Private Sub DisCng(Optional ByVal dis As Double = 0.0)


        If lblRes.Text = String.Empty Then Exit Sub

        CalcDis()

        dis = Double.Parse(lblRes.Text).ToString("#,0.000")

        If chkKm.Checked Then
            'キロ表示
            Me.lblRes.Text = dis.ToString("#,0.000")
            Me.lblM.Text = "Km"
        Else
            Me.lblRes.Text = dis.ToString("#,0")
            Me.lblM.Text = "m"
        End If
    End Sub
#End Region

#Region "コントロール使用可否"
    Private Sub EnableCtl(ByVal flg As Boolean)
        For Each ctl As Control In Me.Controls
            ctl.Enabled = flg
        Next
    End Sub
#End Region

#Region "都道府県設定"
    ''' <summary>
    ''' 都道府県設定
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetPref()
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn

            sb.Remove(0, sb.ToString.Length)
            sb.AppendLine("select pref from pref group by pref")
            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd
            da.Fill(dt1)
            dt2 = dt1.Copy

            Me.cmbPref1.DataSource = dt1
            Me.cmbPref1.DisplayMember = "pref"

            Me.cmbPref2.DataSource = dt2
            Me.cmbPref2.DisplayMember = "pref"

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
            clsComm.Disconn()


        End Try

    End Sub
#End Region

#Region "市区町村設定"
    ''' <summary>
    ''' 市区町村設定
    ''' </summary>
    ''' <param name="cmb"></param>
    ''' <param name="strPref"></param>
    ''' <remarks></remarks>
    Private Sub SetCity(ByVal cmb As ComboBox, ByVal strPref As String)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn

            sb.Remove(0, sb.ToString.Length)

            '//20170619092033 furukawa st ////////////////////////
            '//ふりがなをソートキーとする
            'sb.AppendFormat("select id_pref,city from pref where pref='{0}' group by city", strPref)
            sb.AppendFormat("SELECT ID_PREF,CITY,CITY_K FROM PREF WHERE PREF='{0}' " &
                            "GROUP BY CITY ORDER BY CITY_K", strPref)
            '//20170619092033 furukawa ed ////////////////////////


            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                cmb.DataSource = Nothing
                Exit Sub
            End If

            cmb.DataSource = dt
            cmb.DisplayMember = "city"
            cmb.ValueMember = "id_pref"
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub
#End Region

#Region "街区設定"
    ''' <summary>
    ''' 街区設定
    ''' </summary>
    ''' <param name="cmbArea"></param>
    ''' <param name="strIDPref"></param>
    ''' <remarks></remarks>
    Private Sub SetArea1(ByVal cmbArea As ComboBox, ByVal strIDPref As String)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn



            sb.Remove(0, sb.ToString.Length)

            '2019/08/01furukawa
            sb.AppendFormat("select area1 from {0} where city='{1}' group by area1", clsPref.strPrefDBName, cmbCity1.Text)
            'sb.AppendFormat("select id_area1,area1 from area1 where id_pref='{0}' order by area1_k", strIDPref)

            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd



            '2019/08/01furukawa

            Dim dr As SQLite.SQLiteDataReader
            dr = cmd.ExecuteReader()
            cmbArea.Items.Clear()
            cmbArea.Text = String.Empty
            Do While dr.Read()
                cmbArea.Items.Add(dr("area1"))
            Loop


            'da.Fill(dt)
            'If dt.Rows.Count = 0 Then
            '    cmbArea.DataSource = Nothing
            '    Exit Sub
            'End If
            'cmbArea.DataSource = dt
            'cmbArea.DisplayMember = "area1"
            'cmbArea.ValueMember = "id_area1"


        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    ''' <summary>
    ''' 街区設定
    ''' </summary>
    ''' <param name="cmbArea"></param>
    ''' <param name="cmbCity"></param>
    ''' <remarks></remarks>
    Private Sub SetArea1(ByVal cmbArea As ComboBox, ByVal cmbCity As ComboBox)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn



            sb.Remove(0, sb.ToString.Length)

            sb.AppendFormat("select area1 from {0} where city='{1}' group by area1", clsPref.strPrefDBName, cmbCity.Text)

            cmd.CommandText = sb.ToString
            da.SelectCommand = cmd



            Dim dr As SQLite.SQLiteDataReader
            dr = cmd.ExecuteReader()
            cmbArea.Items.Clear()
            cmbArea.Text = String.Empty
            Do While dr.Read()
                cmbArea.Items.Add(dr("area1"))
            Loop




        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

#End Region

#Region "番地設定"
    ''' <summary>
    ''' 番地設定
    ''' </summary>
    ''' <param name="cmbArea2"></param>
    ''' <param name="cmbArea1"></param>
    ''' <param name="strIDPref"></param>
    ''' <remarks></remarks>
    Private Sub SetArea2(ByVal cmbArea2 As ComboBox,
                         ByVal cmbArea1 As ComboBox,
                         ByVal strIDPref As String)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim clsComm As New clsComm
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn


            sb.Remove(0, sb.ToString.Length)
            sb.AppendFormat("select area2 from {0} " &
                            "where mainflg='1' and area1='{1}' " &
                            "group by area2",
                            clsPref.strPrefDBName, cmbArea1.Text)
            '2019/08/01furukawa
            'sb.AppendFormat("select area2 as ara from DisData " &
            '                "where mainflg='1' and id_pref='{0}' and id_area1='{1}' " &
            '                "group by area2 order by cast(area2 as int)",
            '                strIDPref, cmbArea1.SelectedValue)

            cmd.CommandText = sb.ToString

            Dim dr As SQLite.SQLiteDataReader
            dr = cmd.ExecuteReader()
            cmbArea2.Items.Clear()
            Do While dr.Read()
                cmbArea2.Items.Add(dr("area2").ToString())
            Loop




            '2019/08/01furukawa
            'da.SelectCommand = cmd
            'da.Fill(dt)
            'If dt.Rows.Count = 0 Then
            '    cmbArea2.DataSource = Nothing
            '    Exit Sub
            'End If
            'cmbArea2.DataSource = dt
            'cmbArea2.DisplayMember = "ara"

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub
#End Region
    Dim dt As New DataTable

#Region "検索可能都道府県"
    ''' <summary>
    ''' 検索可能都道府県
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadInit()
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        'Dim dt As New DataTable

        Dim clscomm As New clsComm
        Try

            cn = clscomm.Conn()
            cmd.Connection = cn
            'cmd.CommandText = "select pref,city from pref"
            cmd.CommandText = "select pref from pref group by pref"
            'cmd.CommandText = "select * from pref"
            da.SelectCommand = cmd

            da.Fill(dt)
            Me.lstXY.Items.Clear()

            For r As Integer = 0 To dt.Rows.Count - 1
                Me.lstXY.Items.Add(dt.Rows(r)("pref"))
            Next
        Catch ex As Exception
            MsgBox(ex.Message)

        Finally

            dt.Dispose()
            da.Dispose()
            cmd.Dispose()
            cn.Close()
            clscomm.Disconn()


        End Try


    End Sub
#End Region


#End Region


#Region "未使用"

    Dim id_pref As String
    Dim id_area1 As String

    Private Sub AddressSearchEx()
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim cs As New AutoCompleteStringCollection
        Dim clsComm As New clsComm
        Dim dr As SQLite.SQLiteDataReader
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try
            cn = clsComm.Conn
            cmd.Connection = cn

            sb.Remove(0, sb.ToString.Length)
            Dim arrpref() As Char = {"都", "道", "府", "県"}
            Dim arrcity() As Char = {"市", "区", "町", "村"}

            Select Case True 'txtAdd1.Text.Substring(txtAdd1.Text.Length - 1)


                '県、市がある場合
                Case txtAdd1.Text.IndexOfAny(arrcity) > 0 And txtAdd1.Text.IndexOfAny(arrpref) > 0
                    sb.Remove(0, sb.ToString.Length)
                    sb.AppendFormat("select id_pref from pref where pref || city like '{0}%' " &
                                  "group by city order by city", txtAdd1.Text)

                    cmd.CommandText = sb.ToString
                    id_pref = cmd.ExecuteScalar()

                    If id_pref = String.Empty Then

                        sb.Remove(0, sb.ToString.Length)
                        sb.AppendFormat("select id_pref from pref where pref like '{0}' " &
                                      "group by city order by city", txtAdd1.Text.Substring(0, txtAdd1.Text.IndexOfAny(arrpref) + 1))

                        cmd.CommandText = sb.ToString
                        id_pref = cmd.ExecuteScalar()
                    End If


                    'エリア1がない場合
                    If id_area1 = String.Empty OrElse id_area1 Is Nothing Then


                        sb.Remove(0, sb.ToString.Length)
                        sb.AppendFormat("select id_area1,t.area1 as area from area1 t inner join pref p on t.id_pref=p.id_pref where p.id_pref ='{0}'" &
                                                           " group by t.area1 order by t.area1", id_pref)

                        'sb.AppendFormat("select area1 as area from DisData where id_pref='{0}' group by area1 ", id_pref)
                        cmd.CommandText = sb.ToString
                        da.SelectCommand = cmd
                        da.Fill(dt)
                        If dt.Rows.Count = 0 Then Exit Sub

                        'lst.DataSource = dt
                        'lst.DisplayMember = "area"
                        id_area1 = dt.Rows(0)("id_area1")

                        dr = cmd.ExecuteReader
                        Do While dr.Read
                            cs.Add(Me.txtAdd1.Text & dr("area").ToString)
                        Loop
                        dr.Close()
                        txtAdd1.AutoCompleteCustomSource = cs
                        txtAdd1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                        txtAdd1.AutoCompleteSource = AutoCompleteSource.CustomSource

                        'エリア1がある場合
                    Else
                        sb.Remove(0, sb.ToString.Length)
                        sb.AppendFormat("select t.id_area1 ,t.area1 as area from area1 t where  t.id_area1='{0}'" &
                                                           " group by t.area1 order by t.area1", id_area1)

                        'sb.AppendFormat("select area1 as area from DisData where id_pref='{0}' group by area1 ", id_pref)
                        cmd.CommandText = sb.ToString
                        da.SelectCommand = cmd
                        da.Fill(dt)
                        If dt.Rows.Count = 0 Then Exit Sub
                        id_area1 = dt.Rows(0)("id_area1")

                        'lst.DataSource = dt
                        'lst.DisplayMember = "area"



                        dr = cmd.ExecuteReader
                        Do While dr.Read
                            cs.Add(Me.txtAdd1.Text & dr("area").ToString)
                        Loop
                        dr.Close()
                        txtAdd1.AutoCompleteCustomSource = cs
                        txtAdd1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                        txtAdd1.AutoCompleteSource = AutoCompleteSource.CustomSource

                    End If

                    '県のみの場合
                Case txtAdd1.Text.IndexOfAny(arrpref) > 0
                    sb.AppendFormat("select id_pref, city as city from pref where pref = '{0}' " &
                                    "group by city order by city", txtAdd1.Text)

                    cmd.CommandText = sb.ToString
                    da.SelectCommand = cmd
                    da.Fill(dt)
                    If dt.Rows.Count = 0 Then Exit Sub

                    'lst.DataSource = dt
                    'lst.DisplayMember = "city"

                    id_pref = dt.Rows(0)("id_pref")

                    dr = cmd.ExecuteReader
                    Do While dr.Read
                        cs.Add(txtAdd1.Text & dr("city").ToString)
                    Loop
                    dr.Close()
                    txtAdd1.AutoCompleteCustomSource = cs
                    txtAdd1.AutoCompleteMode = AutoCompleteMode.Suggest
                    txtAdd1.AutoCompleteSource = AutoCompleteSource.CustomSource

                    'area
                    id_area1 = String.Empty


                    'Case Else
                    '    ' Dim arr() As Char = {"都", "道", "府", "県"}
                    '    ' Dim arr2() As Char = {"市", "区", "町", "村"}
                    '    If Me.txtAdd1.Text.IndexOfAny(arrpref) > 0 Then
                    '        '  If Me.txtAdd1.Text.IndexOfAny(arr2) > 0 Then
                    '        ' sb.AppendFormat("select  area2 as area from DisData where pref || city || area1 like '{0}%' group by pref,city,area1", txtAdd1.Text)

                    '        sb.AppendFormat("select  city as area from DisData where pref || city || area1 || area2 like '{0}%' group by pref,city", txtAdd1.Text)

                    '        cmd.CommandText = sb.ToString
                    '        da.SelectCommand = cmd
                    '        da.Fill(dt)
                    '        lst1.DataSource = dt
                    '        lst1.DisplayMember = "area"
                    '        'End If

                    '    End If



            End Select



        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub lst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lst.Click
        txtAdd1.Text &= lst.Text
        AddressSearchEx()

    End Sub

    Private Sub txtAdd1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAdd1.KeyUp
        ' AddressSearchEx()

    End Sub
    Private Sub CreateSource2(ByVal txtAdd As TextBox)
        Dim cn As New SQLite.SQLiteConnection
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim cs As New AutoCompleteStringCollection
        Dim clsComm As New clsComm
        Dim dr As SQLite.SQLiteDataReader
        Dim sb As New System.Text.StringBuilder

        Try
            cn = clsComm.Conn

            sb.Remove(0, sb.ToString.Length)
            sb.AppendLine(" SELECT Z.ZIP ")
            sb.AppendLine(",D.PREF, D.CITY,D.AREA1,D.AREA2 ")
            sb.AppendLine(" FROM DISDATA D ")

            '20160315093345 st ////////////////////////
            'left join ->inner join に変更
            sb.AppendLine(" inner join zip z ")
            'sb.AppendLine(" left join zip z ")
            '20160315093345 ed ////////////////////////

            sb.AppendLine(" ON Z.PREF=D.PREF AND ")
            sb.AppendLine(" Z.CITY=D.CITY AND SUBSTR(Z.AREA,1,1)=SUBSTR(D.AREA1,1,1) ")

            sb.AppendFormat(" WHERE D.PREF='{0}' and d.city='{1}' and d.area1='{2}'",
                            cmbPref1.Text, cmbCity1.Text, cmbArea11.Text)

            cmd.Connection = cn
            cmd.CommandText = sb.ToString

            da.SelectCommand = cmd
            dr = cmd.ExecuteReader
            Do While dr.Read
                cs.Add(dr("pref").ToString & dr("city").ToString &
                       dr("area1").ToString & dr("area2").ToString)
            Loop
            dr.Close()
            txtAdd.AutoCompleteCustomSource = cs
            txtAdd.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            txtAdd.AutoCompleteSource = AutoCompleteSource.CustomSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub
        Finally
            cmd.Dispose()
            clsComm.Disconn()
            clsComm = Nothing
        End Try
    End Sub









#End Region

End Class
