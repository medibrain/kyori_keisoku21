﻿Public Class clsCalcDistance
    'Accessよりそのまま流用

    Public Const PI As Double = 3.14159265358979         '円周率
    Public Const E_FLATTENING As Double = 298.257222101  '扁平率の逆数
    Public Const E_RADIUS_LNG As Double = 6378137        '地球長半径

    '緯度経度より距離（ｍ）を計算する。　緯度、経度は１０進入力値
    ''' <summary>
    ''' 緯度経度より距離を計算する
    ''' </summary>
    ''' <param name="pIdo1">緯度１</param>
    ''' <param name="pKeido1">経度１</param>
    ''' <param name="pIdo2">緯度２</param>
    ''' <param name="pKeido2">経度２</param>
    ''' <returns>メートル</returns>
    ''' <remarks></remarks>
    Public Function ComF_CalDistance(ByVal pIdo1 As Double, _
                                     ByVal pKeido1 As Double, _
                                     ByVal pIdo2 As Double, _
                                     ByVal pKeido2 As Double) As Double
        Dim B10 As Double
        Dim C10 As Double
        Dim D10 As Double
        Dim E10 As Double
        Dim F10 As Double
        Dim G10 As Double
        Dim H10 As Double
        Dim H20 As Double
        Dim I10 As Double
        Dim J10 As Double
        Dim K10 As Double
        Dim L10 As Double

        B10 = RADIANS(pIdo1)
        C10 = RADIANS(pKeido1)
        D10 = RADIANS(pIdo2)
        E10 = RADIANS(pKeido2)
        F10 = B10 - D10          '緯度差
        G10 = C10 - E10          '経度差
        H10 = (B10 + D10) / 2    '緯度平均
        H20 = (Math.Sqrt(2 * E_FLATTENING - 1) / E_FLATTENING) ^ 2

        I10 = Math.Sqrt(1 - H20 * Math.Sin(H10) ^ 2)
        J10 = E_RADIUS_LNG * (1 - H20) / I10 ^ 3
        K10 = E_RADIUS_LNG / I10
        L10 = Math.Sqrt((F10 * J10) ^ 2 + (G10 * K10 * Math.Cos(H10)) ^ 2)
        ComF_CalDistance = ROUNDX(L10, 0)

    End Function

    Private Function RADIANS(ByVal Degrees As Double) As Double
        RADIANS = (PI * Degrees / 180)
    End Function

    '浮動小数点の四捨五入
    Private Function ROUNDX(ByVal SUTI As Double, ByVal KETA As Integer) As Double
        If (SUTI < 0) Then
            ROUNDX = -Int(-SUTI * 10 ^ KETA + 0.5) / 10 ^ KETA
        Else
            ROUNDX = Int(SUTI * 10 ^ KETA + 0.5) / 10 ^ KETA
        End If
    End Function


End Class
