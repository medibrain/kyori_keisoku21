﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.txtAdd1 = New System.Windows.Forms.TextBox()
        Me.txtAdd2 = New System.Windows.Forms.TextBox()
        Me.txtZip1 = New System.Windows.Forms.MaskedTextBox()
        Me.btnImp = New System.Windows.Forms.Button()
        Me.txtZip2 = New System.Windows.Forms.MaskedTextBox()
        Me.btnCalc = New System.Windows.Forms.Button()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.ss = New System.Windows.Forms.StatusStrip()
        Me.tspb = New System.Windows.Forms.ToolStripProgressBar()
        Me.tssl = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnZip = New System.Windows.Forms.Button()
        Me.lblRes = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.dtp = New System.Windows.Forms.DateTimePicker()
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkKm = New System.Windows.Forms.CheckBox()
        Me.lblM = New System.Windows.Forms.Label()
        Me.cmbSejutu = New System.Windows.Forms.ComboBox()
        Me.lst = New System.Windows.Forms.ListBox()
        Me.cmbPref1 = New System.Windows.Forms.ComboBox()
        Me.cmbCity1 = New System.Windows.Forms.ComboBox()
        Me.cmbArea11 = New System.Windows.Forms.ComboBox()
        Me.cmbArea12 = New System.Windows.Forms.ComboBox()
        Me.cmbArea22 = New System.Windows.Forms.ComboBox()
        Me.cmbArea21 = New System.Windows.Forms.ComboBox()
        Me.cmbCity2 = New System.Windows.Forms.ComboBox()
        Me.cmbPref2 = New System.Windows.Forms.ComboBox()
        Me.lstXY = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ss.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtAdd1
        '
        Me.txtAdd1.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtAdd1.Location = New System.Drawing.Point(114, 18)
        Me.txtAdd1.Name = "txtAdd1"
        Me.txtAdd1.Size = New System.Drawing.Size(384, 20)
        Me.txtAdd1.TabIndex = 5
        '
        'txtAdd2
        '
        Me.txtAdd2.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtAdd2.Location = New System.Drawing.Point(114, 44)
        Me.txtAdd2.Name = "txtAdd2"
        Me.txtAdd2.Size = New System.Drawing.Size(383, 20)
        Me.txtAdd2.TabIndex = 11
        '
        'txtZip1
        '
        Me.txtZip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtZip1.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtZip1.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite
        Me.txtZip1.Location = New System.Drawing.Point(38, 18)
        Me.txtZip1.Mask = "000-0000"
        Me.txtZip1.Name = "txtZip1"
        Me.txtZip1.Size = New System.Drawing.Size(72, 20)
        Me.txtZip1.TabIndex = 4
        '
        'btnImp
        '
        Me.btnImp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnImp.Location = New System.Drawing.Point(9, 274)
        Me.btnImp.Name = "btnImp"
        Me.btnImp.Size = New System.Drawing.Size(96, 46)
        Me.btnImp.TabIndex = 7
        Me.btnImp.Text = "位置情報取込"
        Me.btnImp.UseVisualStyleBackColor = True
        '
        'txtZip2
        '
        Me.txtZip2.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtZip2.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtZip2.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite
        Me.txtZip2.Location = New System.Drawing.Point(38, 44)
        Me.txtZip2.Mask = "000-0000"
        Me.txtZip2.Name = "txtZip2"
        Me.txtZip2.Size = New System.Drawing.Size(72, 20)
        Me.txtZip2.TabIndex = 10
        '
        'btnCalc
        '
        Me.btnCalc.Location = New System.Drawing.Point(331, 174)
        Me.btnCalc.Name = "btnCalc"
        Me.btnCalc.Size = New System.Drawing.Size(97, 37)
        Me.btnCalc.TabIndex = 2
        Me.btnCalc.Text = "計測"
        Me.btnCalc.UseVisualStyleBackColor = True
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("MS UI Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("MS UI Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg.DefaultCellStyle = DataGridViewCellStyle2
        Me.dg.Location = New System.Drawing.Point(747, 12)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("MS UI Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dg.RowTemplate.Height = 21
        Me.dg.Size = New System.Drawing.Size(425, 299)
        Me.dg.TabIndex = 6
        '
        'ofd
        '
        Me.ofd.FileName = "OpenFileDialog1"
        Me.ofd.Multiselect = True
        '
        'ss
        '
        Me.ss.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ss.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspb, Me.tssl})
        Me.ss.Location = New System.Drawing.Point(0, 333)
        Me.ss.Name = "ss"
        Me.ss.Size = New System.Drawing.Size(1184, 23)
        Me.ss.TabIndex = 7
        Me.ss.Text = "StatusStrip1"
        '
        'tspb
        '
        Me.tspb.Name = "tspb"
        Me.tspb.Size = New System.Drawing.Size(100, 17)
        '
        'tssl
        '
        Me.tssl.Name = "tssl"
        Me.tssl.Size = New System.Drawing.Size(10, 18)
        Me.tssl.Text = " "
        '
        'btnZip
        '
        Me.btnZip.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnZip.Location = New System.Drawing.Point(116, 274)
        Me.btnZip.Name = "btnZip"
        Me.btnZip.Size = New System.Drawing.Size(96, 46)
        Me.btnZip.TabIndex = 8
        Me.btnZip.Text = "郵便番号取込"
        Me.btnZip.UseVisualStyleBackColor = True
        '
        'lblRes
        '
        Me.lblRes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRes.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblRes.Location = New System.Drawing.Point(60, 172)
        Me.lblRes.Name = "lblRes"
        Me.lblRes.Size = New System.Drawing.Size(215, 40)
        Me.lblRes.TabIndex = 9
        Me.lblRes.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 185)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "結果"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(684, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 12)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "本日のログ"
        '
        'btnLog
        '
        Me.btnLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnLog.Location = New System.Drawing.Point(116, 238)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(96, 30)
        Me.btnLog.TabIndex = 6
        Me.btnLog.Text = "ログ出力"
        Me.btnLog.UseVisualStyleBackColor = True
        '
        'dtp
        '
        Me.dtp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtp.Location = New System.Drawing.Point(4, 242)
        Me.dtp.Name = "dtp"
        Me.dtp.Size = New System.Drawing.Size(105, 19)
        Me.dtp.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "〒"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 12)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "〒"
        '
        'chkKm
        '
        Me.chkKm.AutoSize = True
        Me.chkKm.Location = New System.Drawing.Point(215, 215)
        Me.chkKm.Name = "chkKm"
        Me.chkKm.Size = New System.Drawing.Size(90, 16)
        Me.chkKm.TabIndex = 5
        Me.chkKm.TabStop = False
        Me.chkKm.Text = "1000m→1km"
        Me.chkKm.UseVisualStyleBackColor = True
        '
        'lblM
        '
        Me.lblM.AutoSize = True
        Me.lblM.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblM.Location = New System.Drawing.Point(280, 197)
        Me.lblM.Name = "lblM"
        Me.lblM.Size = New System.Drawing.Size(19, 15)
        Me.lblM.TabIndex = 16
        Me.lblM.Text = "m"
        '
        'cmbSejutu
        '
        Me.cmbSejutu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSejutu.FormattingEnabled = True
        Me.cmbSejutu.Location = New System.Drawing.Point(388, 217)
        Me.cmbSejutu.Name = "cmbSejutu"
        Me.cmbSejutu.Size = New System.Drawing.Size(286, 20)
        Me.cmbSejutu.TabIndex = 17
        Me.cmbSejutu.Visible = False
        '
        'lst
        '
        Me.lst.FormattingEnabled = True
        Me.lst.ItemHeight = 12
        Me.lst.Location = New System.Drawing.Point(326, 217)
        Me.lst.Name = "lst"
        Me.lst.Size = New System.Drawing.Size(57, 28)
        Me.lst.TabIndex = 20
        Me.lst.Visible = False
        '
        'cmbPref1
        '
        Me.cmbPref1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbPref1.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbPref1.FormattingEnabled = True
        Me.cmbPref1.Location = New System.Drawing.Point(12, 18)
        Me.cmbPref1.Name = "cmbPref1"
        Me.cmbPref1.Size = New System.Drawing.Size(95, 23)
        Me.cmbPref1.TabIndex = 0
        '
        'cmbCity1
        '
        Me.cmbCity1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbCity1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCity1.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbCity1.FormattingEnabled = True
        Me.cmbCity1.Location = New System.Drawing.Point(113, 18)
        Me.cmbCity1.Name = "cmbCity1"
        Me.cmbCity1.Size = New System.Drawing.Size(169, 23)
        Me.cmbCity1.TabIndex = 1
        '
        'cmbArea11
        '
        Me.cmbArea11.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbArea11.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbArea11.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbArea11.FormattingEnabled = True
        Me.cmbArea11.Location = New System.Drawing.Point(286, 18)
        Me.cmbArea11.Name = "cmbArea11"
        Me.cmbArea11.Size = New System.Drawing.Size(227, 23)
        Me.cmbArea11.TabIndex = 2
        '
        'cmbArea12
        '
        Me.cmbArea12.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbArea12.FormattingEnabled = True
        Me.cmbArea12.Location = New System.Drawing.Point(519, 18)
        Me.cmbArea12.Name = "cmbArea12"
        Me.cmbArea12.Size = New System.Drawing.Size(90, 23)
        Me.cmbArea12.TabIndex = 3
        '
        'cmbArea22
        '
        Me.cmbArea22.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbArea22.FormattingEnabled = True
        Me.cmbArea22.Location = New System.Drawing.Point(518, 47)
        Me.cmbArea22.Name = "cmbArea22"
        Me.cmbArea22.Size = New System.Drawing.Size(91, 23)
        Me.cmbArea22.TabIndex = 9
        '
        'cmbArea21
        '
        Me.cmbArea21.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbArea21.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbArea21.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbArea21.FormattingEnabled = True
        Me.cmbArea21.Location = New System.Drawing.Point(285, 47)
        Me.cmbArea21.Name = "cmbArea21"
        Me.cmbArea21.Size = New System.Drawing.Size(227, 23)
        Me.cmbArea21.TabIndex = 8
        '
        'cmbCity2
        '
        Me.cmbCity2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbCity2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCity2.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbCity2.FormattingEnabled = True
        Me.cmbCity2.Location = New System.Drawing.Point(112, 47)
        Me.cmbCity2.Name = "cmbCity2"
        Me.cmbCity2.Size = New System.Drawing.Size(169, 23)
        Me.cmbCity2.TabIndex = 7
        '
        'cmbPref2
        '
        Me.cmbPref2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbPref2.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbPref2.FormattingEnabled = True
        Me.cmbPref2.Location = New System.Drawing.Point(12, 47)
        Me.cmbPref2.Name = "cmbPref2"
        Me.cmbPref2.Size = New System.Drawing.Size(95, 23)
        Me.cmbPref2.TabIndex = 6
        '
        'lstXY
        '
        Me.lstXY.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstXY.FormattingEnabled = True
        Me.lstXY.ItemHeight = 12
        Me.lstXY.Location = New System.Drawing.Point(233, 256)
        Me.lstXY.Name = "lstXY"
        Me.lstXY.Size = New System.Drawing.Size(80, 64)
        Me.lstXY.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(231, 242)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 12)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "検索可能"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtZip1)
        Me.GroupBox1.Controls.Add(Me.txtAdd1)
        Me.GroupBox1.Controls.Add(Me.txtAdd2)
        Me.GroupBox1.Controls.Add(Me.txtZip2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 97)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(511, 71)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "郵便番号検索"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbPref1)
        Me.GroupBox2.Controls.Add(Me.cmbPref2)
        Me.GroupBox2.Controls.Add(Me.cmbCity1)
        Me.GroupBox2.Controls.Add(Me.cmbArea11)
        Me.GroupBox2.Controls.Add(Me.cmbArea22)
        Me.GroupBox2.Controls.Add(Me.cmbArea12)
        Me.GroupBox2.Controls.Add(Me.cmbArea21)
        Me.GroupBox2.Controls.Add(Me.cmbCity2)
        Me.GroupBox2.Location = New System.Drawing.Point(15, 10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(632, 83)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "入力検索"
        '
        'lbl1
        '
        Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl1.Location = New System.Drawing.Point(539, 111)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(193, 25)
        Me.lbl1.TabIndex = 25
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl2
        '
        Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl2.Location = New System.Drawing.Point(539, 141)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(193, 25)
        Me.lbl2.TabIndex = 25
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1184, 356)
        Me.Controls.Add(Me.lbl2)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lstXY)
        Me.Controls.Add(Me.lst)
        Me.Controls.Add(Me.cmbSejutu)
        Me.Controls.Add(Me.lblM)
        Me.Controls.Add(Me.chkKm)
        Me.Controls.Add(Me.dtp)
        Me.Controls.Add(Me.btnLog)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRes)
        Me.Controls.Add(Me.btnZip)
        Me.Controls.Add(Me.ss)
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.btnCalc)
        Me.Controls.Add(Me.btnImp)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "距離計測"
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ss.ResumeLayout(False)
        Me.ss.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtAdd1 As System.Windows.Forms.TextBox
    Friend WithEvents txtAdd2 As System.Windows.Forms.TextBox
    Friend WithEvents txtZip1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnImp As System.Windows.Forms.Button
    Friend WithEvents txtZip2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnCalc As System.Windows.Forms.Button
    Friend WithEvents dg As System.Windows.Forms.DataGridView
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ss As System.Windows.Forms.StatusStrip
    Friend WithEvents tspb As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents tssl As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnZip As System.Windows.Forms.Button
    Friend WithEvents lblRes As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnLog As System.Windows.Forms.Button
    Friend WithEvents dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkKm As System.Windows.Forms.CheckBox
    Friend WithEvents lblM As System.Windows.Forms.Label
    Friend WithEvents cmbSejutu As System.Windows.Forms.ComboBox
    Friend WithEvents lst As System.Windows.Forms.ListBox
    Friend WithEvents cmbPref1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCity1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArea11 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArea12 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArea22 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArea21 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCity2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPref2 As System.Windows.Forms.ComboBox
    Friend WithEvents lstXY As System.Windows.Forms.ListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lbl1 As Label
    Friend WithEvents lbl2 As Label
End Class
