﻿Public Class clsComm
    Dim cn As New SQLite.SQLiteConnection
    Dim strdb As String = "data source=" & Application.StartupPath & "\distance.db"
    Dim strTimeOut As String = ";busytimeout=1;defaulttimeout=3"
    Dim strRetry As String = ";PrepareRetries=1"
    Dim strLog As String = "log.txt"


#Region "接続"
    Public Function Conn() As SQLite.SQLiteConnection
        Try
            cn.ConnectionString = strdb & strTimeOut & strRetry

            If cn.State <> ConnectionState.Open Then cn.Open()

            Return cn
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return Nothing
        End Try


    End Function
#End Region

#Region "切断"

    Public Sub Disconn()

        cn.Close()
        cn.Dispose()

    End Sub

#End Region

#Region "ログ出力"

    Public Sub OutLog(ByVal strOutDate As DateTime)
        Dim ts As System.IO.StreamWriter = Nothing
        Dim cmd As New SQLite.SQLiteCommand
        Dim da As New SQLite.SQLiteDataAdapter
        Dim dt As New DataTable

        Try
            frmMain.sfd.Title = "出力するファイル名を指定します"
            frmMain.sfd.Filter = "テキストファイル|*.txt"
            frmMain.sfd.RestoreDirectory = True

            If frmMain.sfd.ShowDialog() = DialogResult.Cancel Then Exit Sub
            Dim sfn As String = frmMain.sfd.FileName

            frmMain.tssl.Text = "ログ出力中"
            cmd.Connection = Conn()
            cmd.CommandText = "select * from log where substr(crdttm,1,10)='" & _
                                strOutDate.ToString("yyyy/MM/dd") & "'" 

            da.SelectCommand = cmd
            da.Fill(dt)

            ts = New System.IO.StreamWriter( _
                sfn, True, System.Text.Encoding.GetEncoding("Shift-JIS"))
            frmMain.tspb.Maximum = dt.Rows.Count
            frmMain.tspb.Minimum = 0
            For r As Integer = 0 To dt.Rows.Count - 1
                For c As Integer = 0 To dt.Columns.Count - 1
                    ts.Write(dt.Rows(r)(c).ToString & ",")
                Next
                ts.Write(vbCrLf)
                frmMain.tspb.Value += 1
            Next
            frmMain.tssl.Text = "ログ出力完了"
        Catch ex As Exception
            MsgBox(ex.Message)

        Finally
            If Not IsNothing(ts) Then ts.Close()
            Disconn()
            frmMain.tspb.Value = 0

        End Try
    End Sub

#End Region


End Class
