﻿Imports System.Data.OleDb

Public Class clsCSV
    Dim olecn As New OleDbConnection
    Dim olecmd As New OleDbCommand
    Dim oleda As New OleDbDataAdapter
    Dim dscsv As New DataSet
    Dim ofd As New OpenFileDialog

    Public Function LoadCSV(ByRef ds As DataSet) As Boolean
        Try
            'CSV選択
            ofd.Title = "位置情報CSVを選択してください"
            ofd.Filter = "CSVファイル|*.csv"
            ofd.Multiselect = True

            If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Function

            'CSVへ接続
            olecn.ConnectionString = "provider=microsoft.jet.oledb.4.0;" & _
            "data source=" & IO.Path.GetDirectoryName(ofd.FileName) & ";" & _
            "extended properties=""TEXT;HDR=YES;FMT=Delimited"""
            olecn.Open()
            olecmd.Connection = olecn

            '選択したCSVをDataSetに格納
            For cnt As Integer = 0 To ofd.FileNames.Count - 1
                frmMain.tssl.Text = IO.Path.GetFileName(ofd.FileNames(cnt)) & "読込"
                System.Windows.Forms.Application.DoEvents()

                olecmd.CommandText = "select * from " & IO.Path.GetFileName(ofd.FileNames(cnt)) & " order by 1,2,3,4,5"
                oleda.SelectCommand = olecmd

                oleda.Fill(dscsv)
                dscsv.Tables(cnt).TableName = IO.Path.GetFileName(ofd.FileNames(cnt))

            Next
            ds = dscsv

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try

    End Function

    Public Sub CloseCSV()
        olecmd.Dispose()
        oleda.Dispose()
        olecn.Close()

    End Sub
End Class
